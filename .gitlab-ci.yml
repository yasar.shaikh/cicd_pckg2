#
# GitLab CI/CD Pipeline for deploying DreamHouse App on Salesforce DX
#

variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task



stages:
 - run-tests
 - Sonar
 - Dev
 - QA
 - UAT
 - Production



#
# Create Scratch Org for code testing -- Stage 1
#
run-tests:
  before_script:
    # Clone salesforce server key repository -- Aded CI runner _ shared 
    # git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/yasar.shaikh/cicd_pckg1.git
    # Decrypt server key
    - pwd 
    - ls
    - openssl enc -aes-256-cbc -md sha256 -salt -d -in assets/server.key.enc -out assets/server.key -k $SERVER_KEY_PASSWORD -pbkdf2
    # Install jq
    - apt update && apt -y install jq
    # Setup SFDX environment variables
    - export CLIURL=https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
    - export SFDX_AUTOUPDATE_DISABLE=false
    - export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
    - export SFDX_DOMAIN_RETRY=300
    - export SFDX_DISABLE_APP_HUB=true
    - export SFDX_LOG_LEVEL=DEBUG
    - export ROOTDIR=force-app/main/default/
    - export TESTLEVEL=RunLocalTests
    - export SCRATCH_ORG_ALIAS=GM_CI
    - export PACKAGEVERSION=""
    # Install Salesforce CLI
    - mkdir sfdx
    - wget -qO- $CLIURL | tar xJ -C sfdx --strip-components 1
    - "./sfdx/install"
    - export PATH=./sfdx/$(pwd):$PATH
    
  stage: run-tests
  script:
  #install dependency plugin
  - echo 'y' | sfdx plugins:install texei-sfdx-plugin
  # Authenticate to the Dev Hub using the server key
  - sfdx force:auth:jwt:grant --setdefaultdevhubusername --setalias HubOrg --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME
  # Create scratch org
  - sfdx force:org:create --targetdevhubusername HubOrg --setdefaultusername --definitionfile config/project-scratch-def.json --setalias $SCRATCH_ORG_ALIAS --wait 10 --durationdays 1
  - sfdx force:org:display --targetusername $SCRATCH_ORG_ALIAS
  #Install dependencies
  - sfdx texei:package:dependencies:install -r -u $SCRATCH_ORG_ALIAS -k 1:IQVIA
  # Push source to scratch org (This is with source code, all files, etc)
  - sfdx force:source:push --targetusername $SCRATCH_ORG_ALIAS
  # DOing some copy paste
  - mkdir -p ./tests/apex
  # Unit Testing
  - sfdx force:apex:test:run -y --targetusername $SCRATCH_ORG_ALIAS --wait 30 --resultformat junit --codecoverage --testlevel $TESTLEVEL --outputdir ./tests/apex > test_results.json

  artifacts:
    reports:
      junit: tests/apex/test-result-*-junit.xml
    paths:
      - tests/
  coverage: '/name="testRunCoverage" value="([\d]+%)"/'
    
  #
  # Create a Scratch Org, create a package/artifact and push into org for testing
  #
  only:
  - master
  - branches
  - merge_requests


Sonar:
  stage: Sonar  

  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - pwd
    - ls
    - cat ./tests/apex/test-result-codecoverage.json
    - sonar-scanner -Dsonar.apex.coverage.reportPath=./tests/apex/test-result-codecoverage.json

    
Dev:
    stage: Dev
    script:
    # Authenticate to the Dev Hub using the server key
    - sfdx force:auth:jwt:grant --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME --setdefaultdevhubusername --setalias HubOrg
    # Authenticate integration sandbox
    - sfdx force:auth:jwt:grant --instanceurl https://test.salesforce.com --clientid $SF_DEV_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME.Dev --setalias lightning
    - export PACKAGE_NAME=$(cat sfdx-project.json  | jq '.packageDirectories[0].package')
    # Create packaged version
    - export PACKAGE_VERSION_ID="$(eval sfdx force:package:version:create --package $PACKAGE_NAME -k $PACKAGE_KEY --wait 30 --json --targetdevhubusername HubOrg | jq '.result.SubscriberPackageVersionId' | tr -d '"')"
    
    - sfdx force:package:install --package $PACKAGE_VERSION_ID -k $PACKAGE_KEY --wait 30 --publishwait 30 --targetusername lightning --noprompt
    # run tests
    - sfdx force:apex:test:run -y --targetusername lightning --wait 120 --resultformat human --codecoverage --testlevel $TESTLEVEL
    when: manual
    only:
    - master
    
    
UAT:
    stage: UAT
    script:
    # Authenticate to the Dev Hub using the server key
    - sfdx force:auth:jwt:grant --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME --setdefaultdevhubusername --setalias HubOrg
    # Authenticate integration sandbox
    - sfdx force:auth:jwt:grant --instanceurl https://test.salesforce.com --clientid $SF_UAT_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME.uat --setalias uat
    - export PACKAGE_NAME=$(cat sfdx-project.json  | jq '.packageDirectories[0].package')
    # Create packaged version
    - export PACKAGE_VERSION_ID="$(eval sfdx force:package:version:create --package $PACKAGE_NAME -k $PACKAGE_KEY --wait 30 --json --targetdevhubusername HubOrg | jq '.result.SubscriberPackageVersionId' | tr -d '"')"
    
    - sfdx force:package:install --package $PACKAGE_VERSION_ID -k $PACKAGE_KEY --wait 30 --publishwait 30 --targetusername uat --noprompt
    # run tests
    - sfdx force:apex:test:run -y --targetusername uat --wait 120 --resultformat human --codecoverage --testlevel $TESTLEVEL
    when: manual
    only:
    - master


Production:
    stage: Production
    script:
    # Authenticate to the Dev Hub using the server key
    - sfdx force:auth:jwt:grant --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME --setdefaultdevhubusername --setalias HubOrg
    - export PACKAGE_NAME=$(cat sfdx-project.json  | jq '.packageDirectories[0].package')
    # Create packaged version
    - export PACKAGE_VERSION_ID="$(eval sfdx force:package:version:create --package $PACKAGE_NAME -k $PACKAGE_KEY --wait 30 --json --targetdevhubusername HubOrg | jq '.result.SubscriberPackageVersionId' | tr -d '"')"
    # promote package
    - sfdx force:package:version:promote --package $PACKAGE_VERSION_ID --noprompt
    # install package
    - sfdx force:package:install --package $PACKAGE_VERSION_ID -k $PACKAGE_KEY --wait 30 --publishwait 30 --targetusername HubOrg --noprompt
    # run tests
    - sfdx force:apex:test:run -y --targetusername HubOrg --wait 120 --resultformat human --codecoverage --testlevel $TESTLEVEL
    when: manual
    only:
    - master