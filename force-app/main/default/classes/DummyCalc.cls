/**
 * @File Name          : DummyCalc.cls
 * @Description        : 
 * @Author             : Yasar Shaikh
 * @Group              : 
 * @Last Modified By   : Yasar Shaikh
 * @Last Modified On   : 13/4/2020, 7:40:53 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/4/2020   Yasar Shaikh     Initial Version
**/
public with sharing class DummyCalc {
    public DummyCalc() {
        System.debug('Hello World');
    }


    
    /**
    * @description 
    * @author Yasar Shaikh | 13/4/2020 
    * @param param1 
    * @param param2 
    * @return Integer 
    **/
    public static Integer dummyCalc(Integer param1, Integer param2){
        return Calc_Utility.add(param1, param2);
    }
}
