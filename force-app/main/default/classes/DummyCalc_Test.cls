/**
 * @File Name          : DummyCalc.cls
 * @Description        : 
 * @Author             : Yasar Shaikh
 * @Group              : 
 * @Last Modified By   : Yasar Shaikh
 * @Last Modified On   : 13/4/2020, 7:40:53 pm
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/4/2020   Yasar Shaikh     Initial Version
**/
@isTest
public with sharing class DummyCalc_Test {

    @isTest
    private static void testDummyCalc(){
        DummyCalc d = new DummyCalc();

        System.assertEquals( 3, DummyCalc.dummyCalc(2,1));
    }
}
